package com.example.gretaqcm;

import com.example.gretaqcm.model.dao.FormationDAO;
import com.example.gretaqcm.model.entity.Formation;
import com.example.gretaqcm.model.entity.Session;
import com.example.gretaqcm.service.SessionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.Normalizer;
import java.util.List;

@SpringBootTest
public class SessionServiceTests {

    private SessionService sessionService;

    private FormationDAO formationDAO;

    @Autowired
    public SessionServiceTests(SessionService sessionService,FormationDAO formationDAO) {
        this.sessionService = sessionService;
        this.formationDAO=formationDAO;
    }

    @Test
    public void getAllTest(){
        List<Session> lesSessions = this.sessionService.getAll();
        Assertions.assertTrue(lesSessions.size() !=0);
        Assertions.assertNotNull(lesSessions);
    }

    @Test
    public void saveCreateTest(){

        List<Session> sessionListT0 = this.sessionService.getAll();
        //Création d'une nouvelle session
     Session newSession= new Session();
     newSession.setNom("test session");
     Formation cda=formationDAO.findById(1L).get();
     Assertions.assertEquals(cda.getNom(),"CDA");
     newSession.setFormation(cda);

        //Sauvegarde de la nouvelle session
     Session savedSession= this.sessionService.save(newSession);
     List<Session> sessionListT1 = this.sessionService.getAll();
     Assertions.assertNotNull(savedSession);
     Assertions.assertEquals(sessionListT0.size()+1,sessionListT1.size());

     // Recupere la session dans la bdd via son id
     Session fetchedSession= this.sessionService.getSession(savedSession.getId());
     Assertions.assertNotNull(fetchedSession);

     //Verification que les attributs de la session recuperer de la bdd == les attributs de la session originale
     Assertions.assertEquals(newSession.getNom(),fetchedSession.getNom());
     Assertions.assertEquals(newSession.getFormation(),fetchedSession.getFormation());
    }
}
