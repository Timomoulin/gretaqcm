package com.example.gretaqcm.controller.admin;

import com.example.gretaqcm.dto.SessionDTO;
import com.example.gretaqcm.model.entity.Session;
import com.example.gretaqcm.service.FormationService;
import com.example.gretaqcm.service.SessionService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class AdminSessionController {
    private SessionService sessionService;
    private FormationService formationService;

    @Autowired
    public AdminSessionController(SessionService sessionService,FormationService formationService) {
        this.sessionService = sessionService;
        this.formationService= formationService;
    }

    @GetMapping("/admin/session")
    public String index(Model model){
        List<Session> sessionList= sessionService.getAll();
        model.addAttribute("sessions",sessionList);
        return "/admin/session/index";
    }

    @GetMapping("/admin/session/{id}")
    public String show(Model model, @PathVariable Long id){
        Session session= sessionService.getSession(id);
        model.addAttribute("uneSession",session);
        return "/admin/session/show";
    }

    @GetMapping("/admin/session/create")
    public String create(Model model){
        model.addAttribute("sessionDTO",new SessionDTO());
        model.addAttribute("formations",formationService.getAllFormation());
        return "/admin/session/create";
    }

    @PostMapping("/admin/session")
    public String store (Model model,@ModelAttribute @Valid SessionDTO sessionDTO, BindingResult bindingResult,RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            model.addAttribute("errorMessage", "Un problème est survenu lros du traitement du formulaire");
            model.addAttribute("formations",formationService.getAllFormation());
            return "admin/session/create";
        }
        Session session = sessionService.toEntity(sessionDTO);
        sessionService.save(session);
        redirectAttributes.addFlashAttribute("successMessage","Ajout de la session "+session.getNom());
        return "redirect:/admin/session";
    }

    @GetMapping("/admin/session/{id}/edit")
    public String edit(Model model,@PathVariable Long id){
        Session session= sessionService.getSession(id);
        SessionDTO sessionDTO= sessionService.toDTO(session);
        model.addAttribute("formations",formationService.getAllFormation());
        model.addAttribute("sessionDTO",sessionDTO);
        return "/admin/session/edit";
    }

    @PostMapping("/admin/session/update")
    public String update (Model model,@ModelAttribute @Valid SessionDTO sessionDTO,BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
            model.addAttribute("formations",formationService.getAllFormation());
            return "/admin/session/edit";
        }
        Session session = sessionService.toEntity(sessionDTO);
        sessionService.save(session);
        redirectAttributes.addFlashAttribute("successMessage","Modification de la session "+session.getNom());
        return "redirect:/admin/session";
    }

    @PostMapping("/admin/session/delete")
    public String delete (@RequestParam Long id,RedirectAttributes redirectAttributes){
        this.sessionService.delete(id);
        redirectAttributes.addFlashAttribute("successMessage","Suppresion de la session");
        return "redirect:/admin/session";
    }
}
