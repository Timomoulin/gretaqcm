package com.example.gretaqcm.controller.admin;

import com.example.gretaqcm.dto.FormationDTO;
import com.example.gretaqcm.model.entity.Formation;
import com.example.gretaqcm.service.FormationService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AdminFormationController {
    private FormationService formationService;
    @Autowired
    public AdminFormationController(FormationService formationService) {
        this.formationService = formationService;
    }

    @GetMapping("/admin/formation")
    public String index(Model model){

        model.addAttribute("formations",this.formationService.getAllFormation());
        return "admin/formation/index";
    }

    @GetMapping("/admin/formation/create")
    public  String create(@ModelAttribute FormationDTO formationDTO){
        return "admin/formation/create";
    }

    @PostMapping("/admin/formation")
    public  String store(@ModelAttribute @Valid FormationDTO formationDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("errorMessage", "Le formulaire comporte une erreur");
            return "admin/formation/create";
        }
        formationService.save(formationDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Inscription réusie");
        return "redirect:/admin/formation";
    }

    @GetMapping("/admin/formation/{id}/edit")
    public String edit(@PathVariable Long id,Model model){
        Formation formation = formationService.findById(id);
        FormationDTO formationDTO = formationService.toDTO(formation);
        model.addAttribute("formationDTO",formationDTO);
        return "/admin/formation/edit";
    }

}
