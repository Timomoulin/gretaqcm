package com.example.gretaqcm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GretaqcmApplication {

    public static void main(String[] args) {
        SpringApplication.run(GretaqcmApplication.class, args);
    }

}
