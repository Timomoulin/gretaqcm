package com.example.gretaqcm.dto;

import com.example.gretaqcm.model.entity.Formation;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public class SessionDTO {
    private Long id;
    @NotBlank(message = "le nom est obligatoire")
    @Size(min = 3,message = "le nom est trop court")
    private String nom;
    private Formation formation;

    public SessionDTO() {
    }


    public SessionDTO(Long id, String nom, Formation formation) {
        this.id = id;
        this.nom = nom;
        this.formation = formation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Formation getFormation() {
        return formation;
    }

    public void setFormation(Formation formation) {
        this.formation = formation;
    }
}
