package com.example.gretaqcm.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public class FormationDTO {
    private Long id;
    @NotBlank(message = "le nom est obligatoire")
    @Size(min = 3,message = "le nom est trop court")
    private String nom;

    public FormationDTO() {
    }

    public FormationDTO(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
