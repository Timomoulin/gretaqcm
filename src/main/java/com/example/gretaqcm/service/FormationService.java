package com.example.gretaqcm.service;

import com.example.gretaqcm.dto.FormationDTO;
import com.example.gretaqcm.model.dao.FormationDAO;
import com.example.gretaqcm.model.entity.Formation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormationService {
    private FormationDAO formationDAO;

    @Autowired
    public FormationService(FormationDAO formationDAO) {
        this.formationDAO = formationDAO;
    }

    public Formation findById(Long id){
        Formation formation= formationDAO.findById(id).orElseThrow();
        return formation;
    }

    /**
     * Méthode qui permet de recuperer les formations
     * @return les formations enregistrer dans la bdd
     * @author timomoulin
     */
    public List<Formation> getAllFormation(){
        return this.formationDAO.findAll();
    }

    public Formation save(FormationDTO formationDTO){
        Formation formation=this.toEntity(formationDTO);
        return formationDAO.save(formation);
    }

    public Formation toEntity(FormationDTO formationDTO){
        Formation resultat;
        if(formationDTO.getId()!=null){
            //TODO lancer une exception si on ne trouve pas la foramtion
            resultat=formationDAO.findById(formationDTO.getId()).orElse(new Formation());
        }
        else{
            resultat=new Formation();
        }
        resultat.setNom(formationDTO.getNom());
        return resultat;
    }

   public FormationDTO toDTO (Formation formation){
        FormationDTO formationDTO=new FormationDTO(formation.getId(), formation.getNom());
        return formationDTO;
   }
}
