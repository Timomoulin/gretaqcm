package com.example.gretaqcm.service;

import com.example.gretaqcm.dto.SessionDTO;
import com.example.gretaqcm.model.dao.SessionDAO;
import com.example.gretaqcm.model.entity.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SessionService {
    private SessionDAO sessionDAO;

@Autowired
    public SessionService(SessionDAO sessionDAO) {
        this.sessionDAO = sessionDAO;
    }

    public List<Session> getAll(){
    List <Session> sessions= sessionDAO.findAll();
    return sessions;
    }

    public Session getSession(Long id){
    Session session= sessionDAO.findById(id).orElseThrow();
    return session;
    }

//    public Session save(SessionDTO sessionDTO){
//        Session session = this.toEntity(sessionDTO);
//       return  sessionDAO.save(session);
//    }

    public  Session save(Session session){
    return sessionDAO.save(session);
    }

    public void delete(Long id) {
        sessionDAO.deleteById(id);
    }


    public Session toEntity(SessionDTO sessionDTO){
    Session session;
    if(sessionDTO.getId() !=null){

        session= sessionDAO.findById(sessionDTO.getId()).orElseThrow();
    }
    else{
        session= new Session();
    }
    session.setFormation(sessionDTO.getFormation());
    session.setNom(sessionDTO.getNom());
    return  session;
    }

    public SessionDTO toDTO(Session session){
    return new SessionDTO(session.getId(), session.getNom(), session.getFormation());
    }
}
