package com.example.gretaqcm.model.entity;


import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui represente une <b>formation</b>
 * @author timomoulin@msn.com
 */
@Entity
public class Formation {
    /**
     * Identifiant de la formation
     */
    @Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * Nom de la formation
     */
    @Column(unique = true,nullable = false)
private  String nom;

    @OneToMany(mappedBy = "formation")
    private List<Session> sessions = new ArrayList<>();

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    /**
     * Constructeur vide de la classe formation
     */
    public Formation() {
    }

    /**
     * Constructeur de la classe formation
     * @param id Id de la formation
     * @param nom Nom de la formation
     */
    public Formation(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode qui permet de comparer deux objets formation
     * @param o la formation a comparer
     * @return vrai si les deux objets sont les mêmes, faux sinon.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Formation formation = (Formation) o;

        if (id != null ? !id.equals(formation.id) : formation.id != null) return false;
        return nom.equals(formation.nom);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + nom.hashCode();
        return result;
    }
}
