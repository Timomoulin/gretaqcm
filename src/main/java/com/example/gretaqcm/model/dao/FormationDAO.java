package com.example.gretaqcm.model.dao;

import com.example.gretaqcm.model.entity.Formation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormationDAO extends JpaRepository<Formation,Long> {


}
