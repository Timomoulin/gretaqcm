package com.example.gretaqcm.model.dao;

import com.example.gretaqcm.model.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionDAO extends JpaRepository<Session, Long> {
}